<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\Candidate;
use App\Models\Employee;
use Spatie\Permission\Models\Role;
use App\Models\LastEducation;
use App\Models\LastPosition;
use App\Models\Skill;
use App\Models\User;
use Carbon\Carbon;

use Mockery;
use Mockery\MockInterface;
use Illuminate\Http\Request;

class EmployeeControllerTest extends TestCase
{
    use WithFaker;
    use WithoutMiddleware;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->master_admin_login = new User($this->userMockData());
        $this->mockUser = Mockery::mock(User::class);
        $this->app->instance(User::class, $this->mockUser);

        $this->employee = new Employee($this->employeeMockData());
        $this->mockEmployee = Mockery::mock(Employee::class);
        $this->app->instance(Employee::class, $this->mockEmployee);
    }

    private function userMockData(): array
    {
        return [
            "id" => 1,
            "email" => "admin@admin.com",
            "password" => \Hash::make('Qwertyuiop123!'),
            "is_admin" => false,
            "remember_token" => null,
            "created_at" => null,
            "updated_at" => "2021-03-23 09:02:49",
            "role_id" => 1,
            "employee_id" => 1,
            "deleted_at" => null
        ];
    }

    private function employeeMockData(): array
    {
        return [
            "id" => 1,
            'name' => 'test',
            'address' => 'test',
            'phone_number' => '08123123123',
        ];
    }

    public function testIndex()
    {
        $this->actingAs($this->master_admin_login);
        $response = $this->get('employee');

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testDatatable()
    {
        $employee = $this->mockEmployee->makePartial();
        $first = $employee->shouldReceive('all')->andReturn(collect($this->employee));

        $this->actingAs($this->master_admin_login);
        $response = $this->get('/datatable/employee');

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testCreate()
    {
        $this->actingAs($this->master_admin_login);
        $response = $this->get('employee/create');

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testStoreSuccess()
    {
        $data  = [
            "email" => "test@test.com",
            "password" => '12345678',
            "password_confirmation" => '12345678',
            "role_id" => 1,
            'name' => 'test',
            "gender" => 'female',
            'address' => 'test',
            'phone_number' => '08123123123',
        ];
        
        $user = $this->mockUser->makePartial();
        $first = $user->shouldReceive('create')->andReturn($this->master_admin_login);
        
        $employee = $this->mockEmployee->makePartial();
        $first = $employee->shouldReceive('create')->andReturn($this->employee);

        $response = $this->actingAs($this->master_admin_login)->postJson(route('employee.store'), $data);

        $response->assertStatus(302);
        $this->assertTrue(true);
    }

    public function testEdit()
    {
        $candidate = $this->mockEmployee->makePartial();
        $first = $candidate->shouldReceive('find')->andReturn($this->employee);
        $this->actingAs($this->master_admin_login);
        $id = encrypt(1);
        $response = $this->get('employee/edit/'.$id);

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testUpdateSuccess()
    {
        $data  = [
            "email" => "test@test.com",
            "password" => '12345678',
            "password_confirmation" => '12345678',
            "role_id" => 1,
            'name' => 'test',
            "gender" => 'female',
            'address' => 'test',
            'phone_number' => '08123123123',
        ];
        
        $user = $this->mockUser->makePartial();
        $first = $user->shouldReceive('update')->andReturn($this->master_admin_login);
        
        $employee = $this->mockEmployee->makePartial();
        $first = $employee->shouldReceive('find')->andReturn($this->employee)->getMock();
        $first = $employee->shouldReceive('update')->andReturn($first);

        $response = $this->actingAs($this->master_admin_login)->postJson(route('employee.update', encrypt(1)), $data);

        $response->assertStatus(302);
        $this->assertTrue(true);
    }

    public function testDelete()
    {
        $candidate = $this->mockEmployee->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn($this->employee)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $id = encrypt(1);
        $response = $this->delete('employee/delete/'.$id);

        $response->assertStatus(302);
        $this->assertTrue(true);
    }

    public function testDeleteNotFound()
    {
        $candidate = $this->mockEmployee->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn(null)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $id = encrypt(1);
        $response = $this->delete('employee/delete/'.$id);

        $response->assertStatus(302);
        $this->assertTrue(true);
    }

    public function testDeleteError()
    {
        $candidate = $this->mockEmployee->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn(null)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $response = $this->delete('employee/delete/1');

        $response->assertStatus(302);
        $this->assertTrue(true);
    }
}