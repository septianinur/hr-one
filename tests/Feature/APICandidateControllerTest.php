<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Http\Requests\CandidateRequest;
use Tests\TestCase;
use App\Models\Candidate;
use App\Models\Employee;
use Spatie\Permission\Models\Role;
use App\Models\LastEducation;
use App\Models\LastPosition;
use App\Models\Skill;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


use Mockery;
use Mockery\MockInterface;
use Illuminate\Http\Request;

class APICandidateControllerTest extends TestCase
{
    use WithFaker;
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
        $this->master_admin_login = new User($this->userMockData());
        $this->mockUser = Mockery::mock(User::class);
        $this->app->instance(User::class, $this->mockUser);

        $this->employee = new Employee($this->employeeMockData());
        $this->mockEmployee = Mockery::mock(Employee::class);
        $this->app->instance(Employee::class, $this->mockEmployee);

        $this->skill = new Skill($this->skillMockData());
        $this->mockSkill = Mockery::mock(Skill::class);
        $this->app->instance(Skill::class, $this->mockSkill);

        $this->candidate = new Candidate($this->candidateMockData());
        $this->mockCandidate = Mockery::mock(Candidate::class);
        $this->app->instance(Candidate::class, $this->mockCandidate);
        $this->mockRequest = Mockery::mock(CandidateRequest::class);

        $this->app->instance(CandidateRequest::class, $this->mockRequest);
    }

    private function userMockData(): array
    {
        return [
            "id" => 1,
            "email" => "admin@admin.com",
            "password" => \Hash::make('Qwertyuiop123!'),
            "is_admin" => false,
            "remember_token" => null,
            "created_at" => null,
            "updated_at" => "2021-03-23 09:02:49",
            "role_id" => 1,
            "employee_id" => 1,
            "deleted_at" => null
        ];
    }

    private function employeeMockData(): array
    {
        return [
            "id" => 1,
            'name' => 'test',
            'address' => 'test',
            'phone_number' => '08123123123',
        ];
    }

    private function candidateMockData(): array
    {
        return [
            'name' => 'test',
            'email' => 'test@test.com',
            'phone_number' => '08123123123',
            'last_education' => 8,
            'education' => 'test',
            'birthdate' => '1997-09-22 00:00:00',
            'experience' => '5 Tahun',
            'last_position' => 81,
            'position_notes' => 'test',
            'applied_position' => 4,
            'top_skill' => json_encode(["1","2","3"]),
            'resume' => 'public\testing.pdf',
            'address' => 'test',
            'gender' => 'female',
        ];
    }

    private function skillMockData(): array
    {
        return [
            "id" => 1,
            'name' => 'laravel',
            'slug' => 'laravel',
        ];
    }

    public function testFetchAll()
    {
        $candidate = $this->mockCandidate->makePartial();
        $paginate = $candidate->shouldReceive('paginate')->withAnyArgs()->andReturn(collect($this->candidate));
        $first = $candidate->shouldReceive('with')->withAnyArgs()->andReturn($paginate);

        $this->actingAs($this->master_admin_login);
        $response = $this->get('/api/candidate');

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testStoreSuccess()
    {
        $file = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'testing.pdf');
        $path_file = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR);

         $uploadedFile = new UploadedFile(
            $file,
            'testing.pdf',
            'application/pdf',
            null,
            true
        );

        $data  = [
            'name' => 'test',
            'email' => 'test@test.com',
            'phone_number' => '08123123123',
            'last_education' => 'diploma-iv-strata-i',
            'education' => 'test',
            'birthdate' => '1997-09-22 00:00:00',
            'experience' => '5 Tahun',
            'last_position' => 'Back End Developer',
            'position_notes' => 'test',
            'applied_position' => 'Senior PHP Developer',
            'top_skill' => 'php, mysql',
            'resume' => $uploadedFile,
            'address' => 'test',
            'gender' => 'female',
        ];
        Storage::fake('public');

        $request = $this->mockRequest->makePartial();
        $first = $request->shouldReceive('all')->andReturn($data);

        $candidateRequest = $this->mockRequest->makePartial();
        $first = $candidateRequest->shouldReceive('has')->andReturn(true);

        $candidate = $this->mockCandidate->makePartial();
        $first = $candidate->shouldReceive('create')->andReturn($this->candidate);

        $response = $this->actingAs($this->master_admin_login)->post('/api/candidate/store', $data);

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testUpdateSuccess()
    {
        $file = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'testing.pdf');
        $path_file = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR);

         $uploadedFile = new UploadedFile(
            $file,
            'testing.pdf',
            'application/pdf',
            null,
            true
        );

        $data  = [
            'name' => 'test',
            'email' => 'test@test.com',
            'phone_number' => '08123123123',
            'last_education' => 'diploma-iv-strata-i',
            'education' => 'test',
            'birthdate' => '1997-09-22 00:00:00',
            'experience' => '5 Tahun',
            'last_position' => 'Back End Developer',
            'position_notes' => 'test',
            'applied_position' => 'Senior PHP Developer',
            'top_skill' => 'php, mysql',
            'resume' => $uploadedFile,
            'address' => 'test',
            'gender' => 'female',
        ];
        Storage::fake('public');

        $request = $this->mockRequest->makePartial();
        $first = $request->shouldReceive('all')->andReturn($data);

        $candidateRequest = $this->mockRequest->makePartial();
        $first = $candidateRequest->shouldReceive('has')->andReturn(true);

        $candidate = $this->mockCandidate->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn($this->candidate)->getMock();
        $first = $candidate->shouldReceive('update')->andReturn($find);

        $response = $this->actingAs($this->master_admin_login)->post('/api/candidate/update/1', $data);

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testDelete()
    {
        $candidate = $this->mockCandidate->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn($this->candidate)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $id = encrypt(1);
        $response = $this->delete('api/candidate/delete/'.$id);

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testDeleteNotFound()
    {
        $candidate = $this->mockCandidate->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn(null)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $id = encrypt(1);
        $response = $this->delete('api/candidate/delete/'.$id);

        $response->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testDeleteError()
    {
        $candidate = $this->mockCandidate->makePartial();
        $find = $candidate->shouldReceive('find')->andReturn(null)->getMock();
        $candidate->shouldReceive('delete')->andReturn($find)->getMock();
        $this->actingAs($this->master_admin_login);
        $response = $this->delete('api/candidate/delete/1');

        $response->assertStatus(200);
        $this->assertTrue(true);
    }
}
