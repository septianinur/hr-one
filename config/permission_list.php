<?php
    return [
        'menu' => [
            [
                'name' => 'Home',
                'slug' => 'home',
                'feature' => [
                    'index' => true,
                    'create' => false,
                    'store' => false,
                    'edit' => false,
                    'update' => false,
                    'delete' => false,
                    'detail' => false,
                ],
            ],[
                'name' => 'Employee',
                'slug' => 'employee',
                'feature' => [
                    'index' => true,
                    'create' => true,
                    'store' => true,
                    'edit' => true,
                    'update' => true,
                    'delete' => true,
                    'detail' => true,
                ],
            ],[
                'name' => 'Candidate',
                'slug' => 'candidate',
                'feature' => [
                    'index' => true,
                    'create' => true,
                    'store' => true,
                    'edit' => true,
                    'update' => true,
                    'delete' => true,
                    'detail' => true,
                ],
            ],
        ]
    ]
?>
