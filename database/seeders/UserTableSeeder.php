<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Employee;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//insert superadmin
		Employee::create([
			'name' => 'Superadmin',
			'address' => 'Bandung',
			'phone_number' => '08123123123',
			'gender' => 'male'
		]);

		$user = User::create([
			'email' => env('DEFAULT_EMAIL', 'admin@admin.com'),
			'password' => \Hash::make(env('DEFAULT_PASSWORD', 12345678)),
			'is_admin' => 1,
			'role_id' => 1,
			'employee_id' => 1
		]);

		$role = Role::find(1);
		$user->assignRole($role);

		//insert senior HRD
		Employee::create([
			'name' => 'John',
			'address' => 'Bandung',
			'phone_number' => '08123123123',
			'gender' => 'male'
		]);

		$user = User::create([
			'email' => 'john@test.com',
			'password' => \Hash::make(env('DEFAULT_PASSWORD', 12345678)),
			'is_admin' => 1,
			'role_id' => 2,
			'employee_id' => 2
		]);

		$role = Role::find(2);
		$user->assignRole($role);

		//insert HRD
		Employee::create([
			'name' => 'Lee',
			'address' => 'Bandung',
			'phone_number' => '08123123123',
			'gender' => 'female'
		]);

		$user = User::create([
			'email' => 'lee@test.com',
			'password' => \Hash::make(env('DEFAULT_PASSWORD', 12345678)),
			'is_admin' => 1,
			'role_id' => 3,
			'employee_id' => 3
		]);

		$role = Role::find(3);
		$user->assignRole($role);
	}
}
