<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use DB;
use Illuminate\Support\Facades\Config;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset cahced roles and permission
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        DB::beginTransaction();

        $roles = [
          [
            'name' => 'Superadmin',
            'slug' => 'superadmin',
          ],[
            'name' => 'Senior HRD',
            'slug' => 'senior-hrd',
          ],[
            'name' => 'HRD',
            'slug' => 'hrd',
          ],[
            'name' => 'Senior PHP Developer',
            'slug' => 'senior-php-developer',
          ],
        ];

        $permissions = Config::get('permission_list.menu');

        foreach ($permissions as $permission) {
            $this->command->info('Seeding Permission for Menu : '.$permission['name']);
            foreach ($permission['feature'] as $name => $value) {
                $this->command->info('Seeding Permission for Menu : '.$permission['slug'].'.'.$name);
                Permission::findOrCreate($permission['slug'].'.'.$name);
            }
        }

        foreach (collect($roles) as $value) {
            $mapping = array();
            $this->command->info('Seeding Permission for Role : '.$value['name']);

            foreach (Permission::all() as $value2) {
              
              if($value2->name == 'home.index'){

                  $mapping[] = $value2->name;
                  $this->command->info('Seeding Permission : '.$value2->name);

              } else if ($value['slug'] == 'superadmin'){

                  $mapping[] = $value2->name;

                  $this->command->info('Seeding Permission : '.$value2->name);
                  
              } else if ($value['slug'] == 'senior-hrd'){
                  if(
                      $value2->name == 'employee.index' ||
                      $value2->name == 'employee.create' ||
                      $value2->name == 'employee.store' ||
                      $value2->name == 'employee.edit' ||
                      $value2->name == 'employee.update' ||
                      $value2->name == 'employee.detail' ||

                      $value2->name == 'candidate.index' ||
                      $value2->name == 'candidate.create' ||
                      $value2->name == 'candidate.store' ||
                      $value2->name == 'candidate.edit' ||
                      $value2->name == 'candidate.update' ||
                      $value2->name == 'candidate.detail'
                  ){

                      $mapping[] = $value2->name;
                      $this->command->info('Seeding Permission : '.$value2->name);

                  }
              }  else if ($value['slug'] == 'hrd'){
                if(
                    $value2->name == 'employee.index' ||
                    $value2->name == 'employee.detail' ||
                    $value2->name == 'candidate.index' ||
                    $value2->name == 'candidate.detail'
                ){

                    $mapping[] = $value2->name;
                    $this->command->info('Seeding Permission : '.$value2->name);

                }

              } 
            }

          $role_name = Role::create(['name' => $value['name'], 'slug' => $value['slug']]);
          $role_name->syncPermissions($mapping);

          DB::commit();
        }
    }
}
