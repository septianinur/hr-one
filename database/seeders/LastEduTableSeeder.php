<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LastEduTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Tidak/Belum Sekolah',
            'Tidak Tamat SD/Sederajat',
            'Tamat SD/Sederajat',
            'SLTP/Sederajat',
            'SLTA/Sederajat',
            'Diploma I/II',
            'Akademi/Diploma III/Sarjana Muda',
            'Diploma IV/Strata I',
            'Strata II',
            'Strata III'
        ];

        foreach ($data as $key => $value) {
            $slug = strtolower(preg_replace('~[^\pL\d]+~u', '-', $value));
            \DB::table('last_educations')->insert([
              'name' => $value,
              'slug' => $slug,
              'description' => null,
              'status' => true,
            ]);
        }

    }
}
