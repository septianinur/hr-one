<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Laravel',
            'Mysql',
            'PostgreSQL',
            'Codeigniter',
            'Python',
            'Java',
            'PHP',
            'SQL',
            'Git',
            'HTML',
            'CSS',
            'JavaScript',
        ];

        foreach ($data as $key => $value) {
            $slug = strtolower(preg_replace('~[^\pL\d]+~u', '-', $value));
            \DB::table('skills')->insert([
              'name' => $value,
              'slug' => $slug,
            ]);
        }
    }
}
