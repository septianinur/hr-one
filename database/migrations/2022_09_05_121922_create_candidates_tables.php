<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('gender',['male', 'female'])->default('male');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('phone_number')->unique();
            $table->integer('last_education');
            $table->string('education');
            $table->string('birthdate');
            $table->string('experience');
            $table->integer('last_position');
            $table->string('position_notes')->nullable();
            $table->integer('applied_position');
            $table->string('top_skill');
            $table->string('resume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
