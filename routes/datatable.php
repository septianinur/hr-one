<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'datatable', 'middleware' => ['auth'], 'as' => 'datatable.'], function () {
    Route::get('role', [App\Http\Controllers\RoleController::class, 'datatable'])->name('role');
    Route::get('employee', [App\Http\Controllers\EmployeeController::class, 'datatable'])->name('employee');
    Route::get('candidate', [App\Http\Controllers\CandidateController::class, 'datatable'])->name('candidate');
});
