<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'App\Http\Controllers\API\v1\AuthController@index');
Route::post('login', 'App\Http\Controllers\API\v1\AuthController@login');
Route::post('register', 'App\Http\Controllers\API\v1\AuthController@register');

Route::group(['prefix' => 'candidate', 'middleware' => 'auth:api'], function(){
    Route::get('/', 'App\Http\Controllers\API\v1\CandidateController@fetchAll');
    Route::post('/store', 'App\Http\Controllers\API\v1\CandidateController@store');
    Route::post('/update/{id}', 'App\Http\Controllers\API\v1\CandidateController@update');
    Route::delete('/delete/{id}', 'App\Http\Controllers\API\v1\CandidateController@delete');
});
