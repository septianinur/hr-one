<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('login', '\App\Http\Controllers\Auth\LoginController@authenticate');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'role', 'middleware' => ['auth','permission'], 'as' => 'role.'], function () {
    Route::get('/', [App\Http\Controllers\RoleController::class, 'index'])->name('index');
    Route::get('/create', [App\Http\Controllers\RoleController::class, 'create'])->name('create');
    Route::post('/store', [App\Http\Controllers\RoleController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [App\Http\Controllers\RoleController::class, 'edit'])->name('edit');
    Route::post('/update/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('update');
    // Route::delete('/delete/{id}', [App\Http\Controllers\RoleController::class, 'delete'])->name('delete');
    Route::get('/status', [App\Http\Controllers\RoleController::class, 'status'])->name('status');
});

Route::group(['prefix' => 'employee', 'middleware' => ['auth','permission'], 'as' => 'employee.'], function () {
    Route::get('/', [App\Http\Controllers\EmployeeController::class, 'index'])->name('index');
    Route::get('/create', [App\Http\Controllers\EmployeeController::class, 'create'])->name('create');
    Route::post('/store', [App\Http\Controllers\EmployeeController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [App\Http\Controllers\EmployeeController::class, 'edit'])->name('edit');
    Route::get('/detail/{id}', [App\Http\Controllers\EmployeeController::class, 'detail'])->name('detail');
    Route::post('/update/{id}', [App\Http\Controllers\EmployeeController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [App\Http\Controllers\EmployeeController::class, 'delete'])->name('delete');
});

Route::group(['prefix' => 'candidate', 'middleware' => ['auth','permission'], 'as' => 'candidate.'], function () {
    Route::get('/', [App\Http\Controllers\CandidateController::class, 'index'])->name('index');
    Route::get('/create', [App\Http\Controllers\CandidateController::class, 'create'])->name('create');
    Route::post('/store', [App\Http\Controllers\CandidateController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [App\Http\Controllers\CandidateController::class, 'edit'])->name('edit');
    Route::get('/detail/{id}', [App\Http\Controllers\CandidateController::class, 'detail'])->name('detail');
    Route::post('/update/{id}', [App\Http\Controllers\CandidateController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [App\Http\Controllers\CandidateController::class, 'delete'])->name('delete');
});
Route::get('/download/{id}', [App\Http\Controllers\CandidateController::class, 'download'])->name('candidate.download')->middleware('auth');