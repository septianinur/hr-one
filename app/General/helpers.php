<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\GeneralSetting;

if (!function_exists('isLogin')) {
    function isLogin()
    {
        return auth()->check();
    }
}

if (!function_exists('user')) {
    function user()
    {
        return auth()->user();
    }
}

if (!function_exists('isAdmin')) {
    function isAdmin()
    {
        $roles = auth()->user()->roles_id === 1 ?? false;
        return $roles ? true : false;
    }
}

if (!function_exists('sanitizeString')) {
    function sanitizeString($string)
    {
        return preg_replace('/[^a-zA-Z0-9_.]/', '_', $string);
    }
}


if (!function_exists('currency_format')) {
    function currency_format($param)
    {
        return "Rp. " . number_format($param, 0, ',', '.');
    }
}