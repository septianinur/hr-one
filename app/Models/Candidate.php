<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'last_education',
        'education',
        'birthdate',
        'experience',
        'last_position',
        'position_notes',
        'applied_position',
        'top_skill',
        'resume',
        'address',
        'gender',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class,'applied_position','id');
    }

    public function lastEducation()
    {
        return $this->belongsTo(LastEducation::class,'last_education','id');
    }

    public function lastPosition()
    {
        return $this->belongsTo(LastPosition::class,'last_position','id');
    }
}
