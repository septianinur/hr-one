<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        if (@$this->id) {
            $this->id = decrypt($this->id);
            return [
                'name' => ['required','string'],
                'email' => ['required', 'string', Rule::unique('users')->ignore($this->id), 'email'],
                'role_id' => ['required'],
                'address' => ['required','string'],
                'phone_number' => ['required','numeric','digits_between:10,13'],
                'gender' => ['required'],
                'password' => ['confirmed', 'min:8', 'nullable'],

            ];
        } else {
            return [
                'name' => ['required','string'],
                'email' => ['required', 'string', Rule::unique('users'), 'email'],
                'role_id' => ['required'],
                'address' => ['required','string'],
                'phone_number' => ['required','numeric','digits_between:10,13'],
                'gender' => ['required'],
                'password' => ['required', 'confirmed', 'min:8'],
            ];
        }
    }
}
