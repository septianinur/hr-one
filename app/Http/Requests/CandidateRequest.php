<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if (@$this->id) {
            $this->id = Request::wantsJson() ? decrypt($this->id) : $this->id;
            return [
                'name' => ['required','string'],
                'email' => ['required', 'string', Rule::unique('candidates')->ignore($this->id), 'email'],
                'address' => ['required','string'],
                'gender' => ['required'],
                'phone_number' => ['required','numeric','digits_between:10,13'],
                'last_education' => ['required'],
                'education' => ['required'],
                'last_position' => ['required'],
                'experience' => ['required'],
                'applied_position' => ['required'],
                'skill' => ['required'],
                'birthdate' => ['required'],
            ];
        } else {
            return [
                'name' => ['required','string'],
                'email' => ['required', 'string', Rule::unique('candidates'), 'email'],
                'address' => ['required','string'],
                'gender' => ['required'],
                'phone_number' => ['required','numeric','digits_between:10,13'],
                'last_education' => ['required'],
                'education' => ['required'],
                'last_position' => ['required'],
                'experience' => ['required'],
                'applied_position' => ['required'],
                'skill' => ['required'],
                'birthdate' => ['required'],
                'resume' => ['required','mimetypes:application/pdf'],
            ];
        }
    }
    public function failedValidation(Validator $validator)
    {
        if (Request::wantsJson()) {
            throw new HttpResponseException(response()->json([
                'code' => 403,
                'error'   => true,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ]));
        }
    }
}
