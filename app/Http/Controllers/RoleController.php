<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    public function index()
    {
        return view('admin.role.index');
    }

    public function datatable(Request $request)
    {
        $data = Role::all();

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('description', function ($data){
                return $data->description ?? '-';
            })
            ->editColumn('created_at', function ($data){
                return Carbon::parse($data->created_at)->timezone('Asia/Jakarta')->format('d-M-Y H:i');
            })
            ->editColumn('status', function ($data){
                $action = '<div class="custom-control custom-switch">
                        <input type="checkbox" class="status" name="status" '.$data->status ? "checked" : "" .' data-bootstrap-switch="" data-off-color="danger" data-on-color="success" >
                        </div>';
                return $action;
            })
            ->addColumn('action', function ($data) {
                $action = '';
                $action = '<a href="' . route('role.edit', encrypt($data->id)) . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Edit Role') . '"><i class="fa fa-edit"></i></a>&nbsp;';
                
                return '<div class="d-flex">' . $action . '</div>';
                
            })
            ->rawColumns(['status','action'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $slug = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->name));

            $req = [
                'name' => $request->name,
                'slug' => $slug,
                'description' => $request->description
            ];

            $save = Role::create($req);

            if(!$save){
                toastr()->error('Gagal menambahkan Role !');
            }

            DB::commit();

            toastr()->success('Berhasil menambahkan Role !');
            return redirect()->route('role.index');
        } catch (\Exception $e) {
            toastr()->error('Gagal menambahkan Role : ' . $e->getMessage());
            Log::info('ROLE-FAILED : failed add role cause : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            DB::rollback();
        } catch (\Throwable $th) {
            toastr()->error('Telah Terjadi Kesalahan. Gagal menambahkan Role !');
            Log::info('ROLE-FAILED : failed add role cause : ' . substr($th->getMessage(), 0, 1000) . ' at ' . $th->getFile() . ' in line ' . $th->getLine());
            DB::rollback();
        }
        return redirect()->back()->withInput();
    }

    public function edit($id)
    {
        $data = Role::find(decrypt($id));
        return view('admin.role.create', ['data' => $data]);
    }

    public function update($id, Request $request)
    {
        try {
            $data = Role::find(decrypt($id));

            DB::beginTransaction();
            $slug = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->name));

            $req = [
                'name' => $request->name,
                'slug' => $slug,
                'description' => $request->description
            ];

            $save = $data->update($req);

            if(!$save){
                toastr()->error('Gagal mengubah Role !');
            }

            DB::commit();

            toastr()->success('Berhasil mengubah Role !');
            return redirect()->route('role.index');
        } catch (\Exception $e) {
            toastr()->error('Gagal mengubah Role : ' . $e->getMessage());
            Log::info('ROLE-FAILED : failed add role cause : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            DB::rollback();
        } catch (\Throwable $th) {
            toastr()->error('Telah Terjadi Kesalahan. Gagal mengubah Role !');
            Log::info('ROLE-FAILED : failed add role cause : ' . substr($th->getMessage(), 0, 1000) . ' at ' . $th->getFile() . ' in line ' . $th->getLine());
            DB::rollback();
        }
        return redirect()->back()->withInput();
    }

    public function status(Request $request)
    {
        $data = Role::find($request->id);
        $status = $data->status ? false : true;
        $data->update(['status' => $status ]);
        return response()->json(['success' => 'true', 'code' => 200]);
    }
}
