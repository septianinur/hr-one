<?php

namespace App\Http\Controllers;

use App\Http\Requests\CandidateRequest;
use Illuminate\Http\Request;
use App\Models\Candidate;
use Spatie\Permission\Models\Role;
use App\Models\LastEducation;
use App\Models\LastPosition;
use App\Models\Skill;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class CandidateController extends Controller
{
    private $candidate;
    public function __construct(Candidate $candidate)
    {
        $this->candidate = $candidate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.candidate.index');
    }

    public function datatable(Request $request)
    {
        $data = $this->candidate->all();
        $skill = Skill::all()->toArray();

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('applied_position', function ($data) {
                return $data->role ? $data->role->name : '-';
            })
            ->editColumn('last_position', function ($data) {
                return $data->lastPosition ? $data->lastPosition->name : '-';
            })
            ->editColumn('last_education', function ($data) {
                return $data->lastEducation ? $data->lastEducation->name : '-';
            })
            ->editColumn('top_skill', function ($data) {
                $result = json_decode($data->top_skill);
                $skill = Skill::whereIn('id',$result)->get();
                $return = '';
                $total = count($result);
                if($total > 0){
                    $i = 1;
                    foreach (@$skill as $key => $value) {
                        $return .= $value->name ?? '-';
                        if($i++ != $total){$return .= ', ';}
                    }
                }
                return $return;
            })
            ->editColumn('resume', function ($data) {
                $action = '<a href="' . route('candidate.download', encrypt($data->id)) . '" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Download Resume') . '"><i class="fa fa-download"></i>&nbsp; Download Resume</a>&nbsp;';
                return $action;
            })
            ->editColumn('created_at', function ($data){
                return Carbon::parse($data->created_at)->timezone('Asia/Jakarta')->format('d-M-Y');
            })
            ->addColumn('action', function ($data) {
                
                $action = '';
                $action .= user()->can('candidate.detail') ? '<a href="' . route('candidate.detail', encrypt($data->id)) . '" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Lihat Kandidat') . '"><i class="fa fa-eye"></i></a>&nbsp;' : '';
                $action .= user()->can('candidate.edit') ? '<a href="' . route('candidate.edit', encrypt($data->id)) . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Edit Kandidat') . '"><i class="fa fa-edit"></i></a>&nbsp;' : '';
                
                $action .= user()->can('candidate.delete') ? '<form method="post" action="' . route('candidate.delete', encrypt($data->id)) . '" style="display: inline">
                    <input type="hidden" name="_token" value="' . csrf_token() . '"><input type="hidden" name="_method" value="delete">
                    <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus data ini?" onclick="return confirm(\'' . __('Hapus Data ini?') . '\')"><i class="fa fa-trash"></i></button>&nbsp;
                    </form>' : '';

                return '<div class="d-flex">' . $action . '</div>';
            })
            ->rawColumns(['resume','action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['last_educations'] = LastEducation::all();
        $data['last_positions'] = LastPosition::all();
        $data['skills'] = Skill::all();
        $data['roles'] = Role::where('id','!=',1)->get();
        return view('admin.candidate.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CandidateRequest $request)
    {
        $redirect = back()->withInput();
        try {
            DB::beginTransaction();

             if ($request->has('resume')) {
                $file = $request->resume;
                $valid_ext = ['pdf'];
                $valid_mime = ['pdf','application/pdf'];

                if (in_array($file->getClientOriginalExtension(), $valid_ext)) {
                    if (in_array($file->getMimeType(), $valid_mime)) {
                        $filename = \Str::random(15) . '_' . sanitizeString($file->getClientOriginalName());
                        $filepath = 'candidate/' . Carbon::now()->timestamp . '/' . \Str::uuid();

                        $path = Storage::disk('local')->putFileAs($filepath, $file, $filename, 'public');

                    } else {
                        throw new Exception("Tipe file tidak sesuai", 500);
                    }
                } else {
                    throw new Exception("Tipe file tidak sesuai", 500);
                }

             }
                
             $data = [
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'last_education' => $request->last_education,
                'education' => $request->education,
                'birthdate' => Carbon::parse($request->birthdate),
                'experience' => $request->experience,
                'last_position' => $request->last_position,
                'position_notes' => $request->position_notes ?? null,
                'applied_position' => $request->applied_position,
                'top_skill' => json_encode($request->skill),
                'resume' => $path,
                'gender' => $request->gender,
                'address' => $request->address,
            ];

            $candidate = $this->candidate->create($data);
            
            if(!$candidate){
                toastr()->error('Gagal menambahkan kandidat !');
                DB::rollback();
            }

            $redirect = redirect()->route('candidate.index');
            toastr()->success('Kandidat baru telah berhasil ditambahkan');
            DB::commit();
        } catch (\Exception $e) {
            Log::info('Data kandidat baru gagal dibuat detail: ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menambahkan kandidat !');
            DB::rollback();
        }
        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data['last_educations'] = LastEducation::all();
        $data['last_positions'] = LastPosition::all();
        $data['skills'] = Skill::all();
        $data['roles'] = Role::where('id','!=',1)->get();
        $data['data'] = $this->candidate->find(decrypt($id));
        $data['data']->birthdate = Carbon::parse($data['data']->birthdate)->format("d M Y");

        $result = json_decode($data['data']->top_skill);
        $skill = Skill::whereIn('id',$result)->get();
        $return = '';
        $total = count($result);
        if($total > 0){
            $i = 1;
            foreach (@$skill as $key => $value) {
                $return .= $value->name ?? '-';
                if($i++ != $total){$return .= ', ';}
            }
        }
        $data['data']->top_skill = $return;
        return view('admin.candidate.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['last_educations'] = LastEducation::all();
        $data['last_positions'] = LastPosition::all();
        $data['skills'] = Skill::all();
        $data['roles'] = Role::where('id','!=',1)->get();
        $data['data'] = $this->candidate->find(decrypt($id));
        $data['data']->birthdate = Carbon::parse($data['data']->birthdate)->format("m/d/y");
        $data['data']->top_skill = json_decode($data['data']->top_skill);
        return view('admin.candidate.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(CandidateRequest $request, $id)
    {
        $redirect = back()->withInput();
        try {
            DB::beginTransaction();
            $candidate = $this->candidate->find(decrypt($id));

             if ($request->has('resume')) {
                $file = $request->resume;
                $valid_ext = ['pdf'];
                $valid_mime = ['pdf','application/pdf'];

                if (in_array($file->getClientOriginalExtension(), $valid_ext)) {
                    if (in_array($file->getMimeType(), $valid_mime)) {
                        $filename = \Str::random(15) . '_' . sanitizeString($file->getClientOriginalName());
                        $filepath = 'candidate/' . Carbon::now()->timestamp . '/' . \Str::uuid();

                        $path = Storage::disk('local')->putFileAs($filepath, $file, $filename, 'public');

                    } else {
                        throw new Exception("Tipe file tidak sesuai", 500);
                    }
                } else {
                    throw new Exception("Tipe file tidak sesuai", 500);
                }

             }
                
             $data = [
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'last_education' => $request->last_education,
                'education' => $request->education,
                'birthdate' => Carbon::parse($request->birthdate),
                'experience' => $request->experience,
                'last_position' => $request->last_position,
                'position_notes' => $request->position_notes ?? null,
                'applied_position' => $request->applied_position,
                'top_skill' => json_encode($request->skill),
                'resume' => $path ?? $candidate->resume,
                'gender' => $request->gender,
                'address' => $request->address,
            ];

            $candidate = $candidate->update($data);
            
            if(!$candidate){
                toastr()->error('Gagal menambahkan kandidat !');
                DB::rollback();
            }

            $redirect = redirect()->route('candidate.index');
            toastr()->success('Kandidat baru telah berhasil ditambahkan');
            DB::commit();
        } catch (\Exception $e) {
            Log::info('Data kandidat baru gagal dibuat detail: ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menambahkan kandidat !');
            DB::rollback();
        }
        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $id = decrypt($id);

            if ($check = $this->candidate->find($id)) {
                $check->delete();
                toastr()->success('Kandidat telah dihapus');
                DB::commit();
            } else {
                toastr()->error('Kandidat tidak ditemukan');
                DB::rollback();
            }
        } catch (\Exception $e) {
            Log::info('Gagal menghapus data kandidat : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menghapus kandidat !');
            DB::rollback();
        }
        return back();
    }

    public function download($id)
    {
        $data = $this->candidate->find(decrypt($id));
        return Storage::download($data->resume,$data->role->slug . '-' . $data->name . '.pdf');
    }
}
