<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    private $employee;
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.employee.index');
    }

    public function datatable(Request $request)
    {
        $data = $this->employee->all();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('role', function ($data) {
                return $data->user->role ? $data->user->role->name : '-';
            })
            ->addColumn('email', function ($data) {
                return $data->user ? $data->user->email : '-';
            })
            ->editColumn('created_at', function ($data){
                return Carbon::parse($data->created_at)->timezone('Asia/Jakarta')->format('d-M-Y');
            })
            ->addColumn('action', function ($data) {
                $action = '';
                $action .= user()->can('employee.edit') ? '<a href="' . route('employee.edit', encrypt($data->id)) . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="' . __('Edit Karyawan') . '"><i class="fa fa-edit"></i></a>&nbsp;' : '';
                
                $action .= user()->can('employee.delete') ? '<form method="post" action="' . route('employee.delete', encrypt($data->id)) . '" style="display: inline">
                    <input type="hidden" name="_token" value="' . csrf_token() . '"><input type="hidden" name="_method" value="delete">
                    <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus data ini?" onclick="return confirm(\'' . __('Hapus Data ini?') . '\')"><i class="fa fa-trash"></i></button>&nbsp;
                    </form>' : '';

                return '<div class="d-flex">' . $action . '</div>';
            })
            ->rawColumns(['status','action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles'] = Role::all();
        return view('admin.employee.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $redirect = back()->withInput();
        try {
            DB::beginTransaction();
             $data = [
                'name' => $request->name,
                'gender' => $request->gender,
                'address' => $request->address,
                'phone_number' => $request->phone_number,
            ];

            $employee = $this->employee->create($data);
            
            if(!$employee){
                toastr()->error('Gagal menambahkan karyawan !');
                DB::rollback();
            }
            
            $data_user = [
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role_id' => $request->role_id,
                'employee_id' => $employee->id
            ];

            $user = User::create($data_user);
            
            if(!$user){
                toastr()->error('Gagal menambahkan karyawan !');
                DB::rollback();
            }

            $redirect = redirect()->route('employee.index');
            toastr()->success('Karyawan baru telah berhasil ditambahkan');
            DB::commit();
        } catch (\Exception $e) {
            Log::info('Data karyawan baru gagal dibuat detail: ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menambahkan karyawan !');
            DB::rollback();
        }
        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function detail(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->employee->find(decrypt($id));
        $data['roles'] = Role::all();

        return view('admin.employee.create', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $redirect = back()->withInput();
            $id = decrypt($id);

            $employee = $this->employee->find($id);

            if($employee){

                $data = [
                    'name' => $request->name,
                    'gender' => $request->gender,
                    'address' => $request->address,
                    'phone_number' => $request->phone_number,
                ];

                $save = $employee->update($data);

                if(!$save){
                    toastr()->error('Gagal mengubah data karyawan !');
                    DB::rollback();
                }
                
                $user = User::where('employee_id', $id)->first();

                if($user){

                    if ($request->password) {
                        $password = Hash::make($request->password);
                    } else {
                        $password = $user->password;
                    }
                
                    $data_user = [
                        'email' => $request->email,
                        'customer' => $request->customer,
                        'password' => $password,
                        'role_id' => $request->role_id,
                        'employee_id' => $employee->id
                    ];
        
                    $save_user = $user->update($data_user);
                    
                    if(!$save_user){
                        toastr()->error('Gagal menambahkan karyawan !');
                        DB::rollback();
                    }

                }else{
                    toastr()->error('Data karyawan tidak ditemukan !');
                    DB::rollback();
                }
            }else{
                toastr()->error('Data karyawan tidak ditemukan !');
                DB::rollback();
            }

            $redirect = redirect()->route('employee.index');
            toastr()->success('Data Karyawan telah berhasil diubah');
            DB::commit();
        } catch (\Exception $e) {
            Log::info('Data karyawan baru gagal diubah detail: ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal mengubah data karyawan !');
            DB::rollback();
        }
        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $id = decrypt($id);

            if ($check = $this->employee->find($id)) {
                if($check->user->id == 1){
                    toastr()->error('Karyawan tidak diizinkan untuk dihapus');
                    DB::rollback();
                }else{
                    $check->delete();
                    User::where('employee_id', $id)->delete();
                    toastr()->success('Karyawan telah dihapus');
                    DB::commit();
                }
            } else {
                toastr()->error('karyawan tidak ditemukan');
                DB::rollback();
            }
        } catch (\Exception $e) {
            Log::info('Gagal menghapus data karyawan : ' . substr($e->getMessage(), 0, 1000) . ' at ' . $e->getFile() . ' in line ' . $e->getLine());
            toastr()->error('Gagal menghapus karyawan !');
            DB::rollback();
        }
        return back();
    }
}
