<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use Spatie\Permission\Models\Role;
use App\Models\LastEducation;
use App\Models\LastPosition;
use App\Models\Skill;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\CandidateRequest;
use Yajra\DataTables\Facades\DataTables;

class CandidateController extends Controller
{
    private $candidate;
    public function __construct(Candidate $candidate)
    {
        $this->candidate = $candidate;
    }
    /**
     * @OA\Get(
     ** path="/candidate",
     *   tags={"Candidate"},
     *   summary="get-candidate",
     *   operationId="GetCandidate",
     * security={ {"api_key_security": {} }},
     * 
     *    @OA\Parameter(
     *      name="page",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *           type="number"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function fetchAll(Request $request)
    {
        try {
            $data = $this->candidate->with('role','lastEducation','lastPosition')->paginate(10, ['*'], 'page', ($request->page ?? 1));

            $header = [
                'code' => 200,
                'error' => false,
                'message' => null
            ];

            return response()->json(['meta' => $header, 'data' => $data]);
        } catch (\Throwable $th) {
            $header = [
                'code' => $th->getCode(),
                'error' => true,
                'message' => $th->getMessage()
            ];

            return response()->json(['meta' => $header, 'data' => null]);
        }
    }
    /**
     * @OA\Post(
     ** path="/candidate/store",
     *   tags={"Candidate"},
     *   summary="save-candidate",
     *   operationId="saveCandidate",
     * security={ {"api_key_security": {} }},
 *
 *     @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="multipart/form-data",
     *       @OA\Schema(
     *         required={"name", "email","phone_number","address","gender","last_education","birthdate","education","experience","last_position","applied_position","skill","resume"},
     *         @OA\Property(property="name", type="string", example="john"),
     *         @OA\Property(property="email", type="string", example="test@test.com"),
     *         @OA\Property(property="phone_number", type="string", example="08221123123"),
     *         @OA\Property(property="address", type="string",example="Bandung"),
     *         @OA\Property(property="gender", enum={"male","female"}, example="male"),
     *         @OA\Property(property="last_education", enum={"Tidak/Belum Sekolah","Tidak Tamat SD/Sederajat","Tamat SD/Sederajat","SLTP/Sederajat","SLTA/Sederajat","Diploma I/II","Akademi/Diploma III/Sarjana Muda","Diploma IV/Strata I","Strata II","Strata III"}, example="Tidak/Belum Sekolah"),
     *         @OA\Property(property="education", type="string", example="Univ. XYZ"),
     *         @OA\Property(property="birthdate", type="string", example="1997-09-22"),
     *         @OA\Property(property="experience", type="string", example="5 Tahun"),
     *         @OA\Property(property="last_position", type="string", example="Back End Developer"),
     *         @OA\Property(property="applied_position", enum={"Senior HRD","HRD","Senior PHP Developer"}, example="Senior PHP Developer"),
     *         @OA\Property(property="skill", type="string", example="php, mysql"),
     *         @OA\Property(property="resume", type="string", format="binary"),
     *       )
     *     )
 *       
 *      ),
     *    @OA\Response(
     *      response=200,
     *       description="Success",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(CandidateRequest $request)
    {
        $message = null;
        try {
            DB::beginTransaction();

            if ($request->has('resume')) {
                $file = $request->resume;
                $valid_ext = ['pdf'];
                $valid_mime = ['pdf','application/pdf'];

                if (in_array($file->getClientOriginalExtension(), $valid_ext)) {
                    if (in_array($file->getMimeType(), $valid_mime)) {
                        $filename = \Str::random(15) . '_' . sanitizeString($file->getClientOriginalName());
                        $filepath = 'candidate/' . Carbon::now()->timestamp . '/' . \Str::uuid();

                        $path = Storage::disk('local')->putFileAs($filepath, $file, $filename, 'public');

                    } else {
                        throw new Exception("Tipe file tidak sesuai", 500);
                    }
                } else {
                    throw new Exception("Tipe file tidak sesuai", 500);
                }

            }
            
            $slug_education = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->last_education));
            $lastEducation = LastEducation::where('slug', $slug_education)->first();
            if(!$lastEducation){
                $lastEducation = LastPosition::create([
                    'name' => $request->last_education,
                    'slug' => $slug_education,
                    'description' => null,
                    'status' => true,
                ]);
            }

            $slug_position = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->last_position));
            $lastPosition = LastPosition::where('slug', $slug_position)->first();
            if(!$lastPosition){
                $lastPosition = LastPosition::create([
                    'name' => $request->last_position,
                    'slug' => $slug_position,
                    'description' => null,
                    'status' => true,
                ]);
            }

            $slug_applied = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->applied_position));
            $appliedPosition = Role::where('slug', $slug_applied)->first();
            if(!$appliedPosition){
                $appliedPosition = Role::create([
                    'name' => $request->applied_position,
                    'slug' => $slug_position,
                    'description' => null,
                    'status' => true,
                ]);
            }

            $skill = explode(",", $request->skill);

                
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'last_education' => $lastPosition->id ?? 5,
                'education' => $request->education,
                'birthdate' => Carbon::parse($request->birthdate),
                'experience' => $request->experience,
                'last_position' => $lastPosition->id ?? 81,
                'position_notes' => $request->position_notes ?? null,
                'applied_position' => $appliedPosition->id ?? 4,
                'top_skill' => json_encode($skill) ?? null,
                'resume' => $path ?? '-',
                'gender' => $request->gender,
                'address' => $request->address,
            ];

            $candidate = $this->candidate->create($data);
            
            if(!$candidate){
                $message = 'Gagal menambahkan kandidat !';
                DB::rollback();
            }
            DB::commit();
            
            $header = [
                'code' => 200,
                'error' => false,
                'message' => null
            ];

            return response()->json(['meta' => $header, 'data' => $data]);

        } catch (\Throwable $th) {
            $header = [
                'code' => $th->getCode(),
                'error' => true,
                'message' => $th->getMessage()
            ];

            return response()->json(['meta' => $header, 'data' => null]);
        }
    }
    /**
     * @OA\Post(
     ** path="/candidate/update/{id}",
     *   tags={"Candidate"},
     *   summary="update-candidate",
     *   operationId="updateCandidate",
     * security={ {"api_key_security": {} }},
     * 
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="number"
     *      )
     *   ),
 *
 *     @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="multipart/form-data",
     *       @OA\Schema(
     *         @OA\Property(property="name", type="string"),
     *         @OA\Property(property="email", type="string"),
     *         @OA\Property(property="phone_number", type="string"),
     *         @OA\Property(property="address", type="string"),
     *         @OA\Property(property="gender", enum={"male","female"}),
     *         @OA\Property(property="last_education", enum={"Tidak/Belum Sekolah","Tidak Tamat SD/Sederajat","Tamat SD/Sederajat","SLTP/Sederajat","SLTA/Sederajat","Diploma I/II","Akademi/Diploma III/Sarjana Muda","Diploma IV/Strata I","Strata II","Strata III"}),
     *         @OA\Property(property="education", type="string"),
     *         @OA\Property(property="birthdate", type="string"),
     *         @OA\Property(property="experience", type="string"),
     *         @OA\Property(property="last_position", type="string"),
     *         @OA\Property(property="applied_position", enum={"Senior HRD","HRD","Senior PHP Developer"}),
     *         @OA\Property(property="skill", type="string"),
     *         @OA\Property(property="resume", type="string", format="binary"),
     *       )
     *     )
 *       
 *      ),
     *    @OA\Response(
     *      response=200,
     *       description="Success",
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function update(CandidateRequest $request, $id)
    {
        $message = null;
        try {
            DB::beginTransaction();
            $candidate = $this->candidate->find($id);
            if($candidate) {

                if(isset($request->resume) || !empty($request->resume)){
                    $file = $request->resume;
                    $valid_ext = ['pdf'];
                    $valid_mime = ['pdf','application/pdf'];

                    if (in_array($file->getClientOriginalExtension(), $valid_ext)) {
                        if (in_array($file->getMimeType(), $valid_mime)) {
                            $filename = \Str::random(15) . '_' . sanitizeString($file->getClientOriginalName());
                            $filepath = 'candidate/' . Carbon::now()->timestamp . '/' . \Str::uuid();

                            $path = Storage::disk('local')->putFileAs($filepath, $file, $filename, 'public');

                        } else {
                            throw new Exception("Tipe file tidak sesuai", 500);
                        }
                    } else {
                        throw new Exception("Tipe file tidak sesuai", 500);
                    }

                }
                
                if(isset($request->last_education) || !empty($request->last_education)){
                    $slug_education = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->last_education));
                    $lastEducation = LastEducation::where('slug', $slug_education)->first();
                    if(!$lastEducation){
                        $lastEducation = LastPosition::create([
                            'name' => $request->last_education,
                            'slug' => $slug_education,
                            'description' => null,
                            'status' => true,
                        ]);
                    }
                }

                if(isset($request->last_position) || !empty($request->last_position)){
                    $slug_position = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->last_position));
                    $lastPosition = LastPosition::where('slug', $slug_position)->first();
                    if(!$lastPosition){
                        $lastPosition = LastPosition::create([
                            'name' => $request->last_position,
                            'slug' => $slug_position,
                            'description' => null,
                            'status' => true,
                        ]);
                    }
                }

                if(isset($request->applied_position) || !empty($request->applied_position)){
                    $slug_applied = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->applied_position));
                    $appliedPosition = Role::where('slug', $slug_applied)->first();
                    if(!$appliedPosition){
                        $appliedPosition = Role::create([
                            'name' => $request->applied_position,
                            'slug' => $slug_position,
                            'description' => null,
                            'status' => true,
                        ]);
                    }
                }

                if(isset($request->skill) || !empty($request->skill)){
                    $data = explode(",", $request->skill);
                    $skill = [];
                    foreach($data as $value){
                        $search = Skill::where('name', trim($value))->first();
                        if($search){
                            $skill[] = $search->id;
                        }else{
                            $slug = strtolower(preg_replace('~[^\pL\d]+~u', '-', $request->value));
                            $data = Skill::create([
                                'name' => $value,
                                'slug' => $slug
                            ]);
                            $skill[] = $data->id;
                        }
                    }
                }


                    
                $data = [
                    'name' => isset($request->name) || !empty($request->name) ? $request->name : $candidate->name,
                    'email' => isset($request->email) || !empty($request->email) ? $request->email : $candidate->email,
                    'phone_number' => isset($request->phone_number) || !empty($request->phone_number) ? $request->phone_number : $candidate->phone_number,
                    'last_education' => isset($request->last_education) || !empty($request->last_education) ? ($lastPosition->id ?? 5) : $candidate->last_education,
                    'education' => isset($request->education) || !empty($request->education) ? $request->education : $candidate->education,
                    'birthdate' => isset($request->birthdate) || !empty($request->birthdate) ? Carbon::parse($request->birthdate) : $candidate->birthdate,
                    'experience' => isset($request->experience) || !empty($request->experience) ? ($request->experience) : $candidate->experience,
                    'last_position' =>  isset($request->last_position) || !empty($request->last_position) ? ($lastPosition->id ?? 81) : $candidate->last_position,
                    'position_notes' => $request->position_notes ?? null,
                    'applied_position' =>  isset($request->last_position) || !empty($request->last_position) ? ($appliedPosition->id ?? 4) : $candidate->applied_position,
                    'top_skill' => isset($request->skill) || !empty($request->skill) ? (json_encode($skill) ?? null) : $candidate->top_skill,
                    'resume' => $path ?? $candidate->resume,
                    'gender' => isset($request->gender) || !empty($request->gender) ? $request->gender : $candidate->gender,
                    'address' => isset($request->address) || !empty($request->address) ? $request->address : $candidate->address,
                ];

                $candidate = $candidate->update($data);
                
                if(!$candidate){
                    $message = 'Gagal menambahkan kandidat !';
                    DB::rollback();
                }
                DB::commit();
                
                $header = [
                    'code' => 200,
                    'error' => false,
                    'message' => null
                ];

                return response()->json(['meta' => $header, 'data' => $data]);
                
            }else{
                
                throw new Exception("Kandidat tidak ditemukan", 404);
            }

        } catch (\Throwable $th) {
            $header = [
                'code' => $th->getCode(),
                'error' => true,
                'message' => $th->getMessage()
            ];

            return response()->json(['meta' => $header, 'data' => null]);
        }
    }
    /**
     * @OA\Delete(
     ** path="/candidate/delete/{id}",
     *   tags={"Candidate"},
     *   summary="delete-candidate",
     *   operationId="deleteCandidate",
     * security={ {"api_key_security": {} }},
     * 
     *    @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="number"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    public function delete($id)
    {
        $message = null;
        try {
            DB::beginTransaction();

            if ($check = $this->candidate->find($id)) {
                $check->delete();
                DB::commit();
                

                $header = [
                    'code' => 200,
                    'error' => false,
                    'message' => 'Data kandidat berhasil dihapus'
                ];

                return response()->json(['meta' => $header, 'data' => null]);
            } else {
                throw new Exception("Kandidat tidak ditemukan", 404);
                DB::rollback();
            }
        } catch (\Throwable $th) {
            Log::info('Gagal menghapus data kandidat : ' . substr($th->getMessage(), 0, 1000) . ' at ' . $th->getFile() . ' in line ' . $th->getLine());
            DB::rollback();
            $header = [
                'code' => $th->getCode(),
                'error' => true,
                'message' => $th->getMessage()
            ];

            return response()->json(['meta' => $header, 'data' => null]);
        }
    }
}
