@extends("layout.template")

@push('custom-css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<style>
    .swal-modal{
        width: 375px !important;
    }
    .swal-footer{
        text-align: center !important;
    }
    .img-preview{
      width: 5rem;
    }
    .address {
      resize: none;
    }
</style>
@endpush

@section('content')

@if (old())
  {{-- expr --}}
  {{-- @dd(old()) --}}
@endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>{{ (@$data) ? 'Ubah' : 'Tambah' }} Posisi</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                          <form action="{{ (@$data) ? route('role.update', encrypt(@$data->id)) : route('role.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control" id="name" value="{{ (@$data->name) ?? old('name') }}" placeholder="Nama" required>
                                  {!! @$errors ? $errors->first('name', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-name"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Deskripsi</label>
                                <div class="col-sm-10">
                                  <textarea name="description" class="form-control description" id="description" placeholder="Deskripsi" cols="3" rows="3">{{ (@$data->description) ?? old('description') }}</textarea>
                                  {!! @$errors ? $errors->first('description', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-description"></strong>
                                    </span>
                                </div>
                              </div>
                            </div>

                            <div class="card-footer">
                              <a href="{{ route('role.index') }}" id="batal" class="btn btn-default">Batal</a>
                              <button type="submit" class="btn btn-primary">{{ (@$data) ? 'Ubah' : 'Simpan' }}</button>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')

@include("admin.role.script")
@endpush
