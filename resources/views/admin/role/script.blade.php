<!-- DataTables  & Plugins -->
<script src="{{ asset('template/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/jszip/jszip.min.js"></script>
<script src="{{ asset('template/') }}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{ asset('template/') }}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();

  $('#roles-table').DataTable({
    processing: true,
    "language": { "emptyTable": "Tidak ada data",
                },
    serverSide: true,
    ajax: '{{ route("datatable.role") }}',
    columns: [
      {data: 'DT_RowIndex', name: 'no', orderable: false, searchable: false},
      {data: 'name', name: 'name'},
      {data: 'slug', name: 'slug'},
      {data: 'description', name: 'description'},
      {data: 'status', name: 'status'},
      {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
      {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
  });


  // $(".status").change(function() {
  //   $.ajax({
  //     url: "{{ route('role.status')}}",
  //     type:'GET',
  //     data: {
  //       id : $(this).data('id'),
  //     },
  //     success: function(result){
        
  //     }
  //   });
// });
});
</script>
