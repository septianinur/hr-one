@extends("layout.template")

@push('custom-css')
<link rel="stylesheet" type="text/css" href="{{ asset('template/') }}/plugins/datepicker/bootstrap-datepicker.min.css" />
<style>
    .swal-modal{
        width: 375px !important;
    }
    .swal-footer{
        text-align: center !important;
    }
    .img-preview{
      width: 5rem;
    }
    .address {
      resize: none;
    }
</style>
@endpush

@section('content')

@if (old())
  {{-- expr --}}
  {{-- @dd(old()) --}}
@endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>{{ (@$data) ? 'Ubah' : 'Buat' }} Kandidat</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                          <form action="{{ (@$data) ? route('candidate.update', encrypt(@$data->id)) : route('candidate.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control" id="name" value="{{ (@$data->name) ?? old('name') }}" placeholder="Nama" required>
                                  {!! @$errors ? $errors->first('name', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-name"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Alamat Email<span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="email" name="email" class="form-control" value="{{ (@$data->email) ?? old('email') }}" id="email" placeholder="Alamat Email">
                                  {!! @$errors ? $errors->first('email', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-email"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="address" class="col-sm-2 col-form-label">Alamat<span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <textarea name="address" class="form-control address" id="address" placeholder="Alamat" cols="3" rows="3">{{ (@$data->address) ?? old('address') }}</textarea>
                                  {!! @$errors ? $errors->first('address', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-address"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="birthdate" class="col-sm-2 col-form-label">Tanggal Lahir <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="birthdate" class="form-control" id="birthdate" value="{{ (@$data->birthdate) ?? old('birthdate') }}" placeholder="Tanggal Lahir" required>
                                  {!! @$errors ? $errors->first('birthdate', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-birthdate"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="gender" class="col-sm-2 col-form-label">Jenis Kelamin <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="gender" class="form-control" id="gender" required>
                                    <option value="" disabled {{ (@$data->gender) ?? 'selected' }}>Jenis Kelamin</option>
                                    <option value="male" {{ ((old('gender') == "male") || ((@$data->gender) == "male")) ? 'selected' : '' }}>Pria</option>
                                    <option value="female" {{ ((old('gender') == "female") || ((@$data->gender) == "female")) ? 'selected' : '' }}>Wanita</option>
                                  </select>
                                  {!! @$errors ? $errors->first('gender', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-gender"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="phone_number" class="col-sm-2 col-form-label">No Telepon <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="tel" name="phone_number" class="form-control" id="phone_number"  maxlength="13" minlength="10" value="{{ (@$data->phone_number) ?? old('phone_number') }}" placeholder="No Telepon" required>
                                  {!! @$errors ? $errors->first('phone_number', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-phone_number"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="last_education" class="col-sm-2 col-form-label">Tingkat Pendidikan Terakhir <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="last_education" id="last_education" class="form-control">
                                    <option value="" disabled selected> Pilih Pendidikan Terakhir </option>
                                    @foreach ($last_educations as $value)
                                      <option value="{{ @$value->id }}"  {{ ((old('last_education') == $value->id) || ((@$data->last_education) == $value->id)) ? 'selected' : '' }} >{{ @$value->name }}</option>
                                    @endforeach
                                  </select>
                                  {!! @$errors ? $errors->first('last_education', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-last_education"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="education" class="col-sm-2 col-form-label">Pendidikan Terakhir <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="education" class="form-control" id="education" value="{{ (@$data->education) ?? old('education') }}" placeholder="Pendidikan Terakhir" required>
                                  {!! @$errors ? $errors->first('education', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-education"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="last_position" class="col-sm-2 col-form-label">Posisi Terakhir <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="last_position" id="last_position" class="form-control">
                                    <option value="" disabled selected> Pilih Posisi Terakhir </option>
                                    @foreach ($last_positions as $value)
                                      <option value="{{ @$value->id }}"  {{ ((old('last_position') == $value->id) || ((@$data->last_position) == $value->id)) ? 'selected' : '' }} >{{ @$value->name }}</option>
                                    @endforeach
                                  </select>
                                  {!! @$errors ? $errors->first('last_position', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-last_position"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="experience" class="col-sm-2 col-form-label">Pengalaman (Dalam Tahun) <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="experience" class="form-control" id="experience" value="{{ (@$data->experience) ?? old('experience') }}" placeholder="Pengalaman (Dalam Tahun)" required>
                                  {!! @$errors ? $errors->first('experience', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-experience"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="applied_position" class="col-sm-2 col-form-label">Applied Positions <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="applied_position" id="applied_position" class="form-control">
                                    <option value="" disabled selected> Choose Applied Positions </option>
                                    @foreach ($roles as $value)
                                      <option value="{{ @$value->id }}"  {{ ((old('applied_position') == $value->id) || ((@$data->applied_position) == $value->id)) ? 'selected' : '' }} >{{ @$value->name }}</option>
                                    @endforeach
                                  </select>
                                  {!! @$errors ? $errors->first('applied_position', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-applied_position"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="skill" class="col-sm-2 col-form-label">Skill <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="skill[]" id="skill" class="form-control"  multiple="multiple">
                                    @foreach ($skills as $value)
                                      <option value="{{ @$value->id }}" {{ @$data ? ((in_array($value->id, @$data->top_skill)) ? 'selected' : '') : '' }} >{{ @$value->name }}</option>
                                    @endforeach
                                  </select>
                                  {!! @$errors ? $errors->first('skill', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-skill"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="resume" class="col-sm-2 col-form-label">Resume {!! @$data ? '' : '<span class="required-field-sign"></span>' !!}</label>
                                <div class="col-sm-10">
                                  <input type="file" name="resume" class="form-control" id="resume" value="{{ (@$data->resume) ?? old('resume') }}" placeholder="Resume"  accept="application/pdf" {{@$data ? '' : 'required'}}>
                                  {!! @$errors ? $errors->first('resume', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-resume"></strong>
                                    </span>
                                </div>
                              </div>
                            </div>

                            <div class="card-footer">
                              <a href="{{ route('candidate.index') }}" id="batal" class="btn btn-default">Batal</a>
                              <button type="submit" class="btn btn-primary">{{ (@$data) ? 'Ubah' : 'Simpan' }}</button>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')

<script>
  $(document).ready(function(){
    $("#phone_number").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if (event.which > 31 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
  });

</script>

@include("admin.candidate.script")
@endpush
