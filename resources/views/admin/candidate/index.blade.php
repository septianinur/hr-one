@extends("layout.template")

@push('custom-css')
<!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('template/') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <style>
        div.dataTables_wrapper div.dataTables_filter input {
            padding-right: 1%
        }
    </style>
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>Data Kandidat</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                @can('candidate.create')
                                    <div class="row">
                                        <a href="{{ route('candidate.create') }}" class="btn btn-success" id="btn-upload">
                                            <i class="fa fa-plus"></i> Tambah Kandidat
                                        </a>
                                    </div>
                                @endcan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="candidates-table" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                    <th>No</th>
                                    <th>Applied Position</th>
                                    <th>Last Position</th>
                                    <th>Last Education</th>
                                    <th>Top Skill</th>
                                    <th>Name</th>
                                    <th>Resume</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')
@include("admin.candidate.script")
@endpush
