@extends("layout.template")

@push('custom-css')
<link rel="stylesheet" type="text/css" href="{{ asset('template/') }}/plugins/datepicker/bootstrap-datepicker.min.css" />
<style>
    .swal-modal{
        width: 375px !important;
    }
    .swal-footer{
        text-align: center !important;
    }
    .img-preview{
      width: 5rem;
    }
    .address {
      resize: none;
    }
</style>
@endpush

@section('content')

@if (old())
  {{-- expr --}}
  {{-- @dd(old()) --}}
@endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>Detail Kandidat</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->name ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Alamat Email</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->email ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->address ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="birthdate" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->birthdate ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="gender" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->gender ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="phone_number" class="col-sm-2 col-form-label">No Telepon</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->phone_number ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="last_education" class="col-sm-2 col-form-label">Tingkat Pendidikan Terakhir</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->lastEducation->name ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="education" class="col-sm-2 col-form-label">Pendidikan Terakhir</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->education ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="last_position" class="col-sm-2 col-form-label">Posisi Terakhir</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->lastPosition->name ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="experience" class="col-sm-2 col-form-label">Pengalaman (Dalam Tahun)</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->experience ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="applied_position" class="col-sm-2 col-form-label">Applied Positions</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->role->name ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="skill" class="col-sm-2 col-form-label">Skill</label>
                                <div class="col-sm-10">
                                  <p>{{ @$data->top_skill ?? '-' }}</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="resume" class="col-sm-2 col-form-label">Resume</label>
                                <div class="col-sm-10">
                                  <a href="'{{route('candidate.download', encrypt($data->id))}} '" class="btn btn-success btn-xs"><i class="fa fa-download"></i>&nbsp; Download Resume</a>&nbsp;
                                </div>
                              </div>
                            </div>

                            <div class="card-footer">
                              <a href="{{ route('candidate.index') }}" id="batal" class="btn btn-default">Batal</a>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')

<script>
  $(document).ready(function(){
    $("#phone_number").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if (event.which > 31 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
  });

</script>

@include("admin.candidate.script")
@endpush
