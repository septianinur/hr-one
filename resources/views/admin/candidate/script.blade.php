<!-- DataTables  & Plugins -->
<script src="{{ asset('template/') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('template/') }}/plugins/jszip/jszip.min.js"></script>
<script src="{{ asset('template/') }}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{ asset('template/') }}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="{{ asset('js') }}/moment.min.js"></script>
<script src="{{ asset('template/') }}/plugins/datepicker/bootstrap-datepicker.min.js"></script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();

  $('#candidates-table').DataTable({
    processing: true,
    "language": { "emptyTable": "Tidak ada data",
                },
    serverSide: true,
    ajax: '{{ route("datatable.candidate") }}',
    columns: [
      {data: 'DT_RowIndex', name: 'no', orderable: false, searchable: false},
      {data: 'applied_position', name: 'applied_position'},
      {data: 'last_position', name: 'last_position'},
      {data: 'last_education', name: 'last_education'},
      {data: 'top_skill', name: 'top_skill'},
      {data: 'name', name: 'name'},
      {data: 'resume', name: 'resume'},
      {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
      {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    initComplete: function(){
        var api = this.api();
        $('#candidates-table_filter input')
            .off('.DT')
            .on('keyup.DT', function (e) {
                if (e.keyCode == 13) {
                    api.search(this.value).draw();
                }
            });
      },
  });

  var date = new Date();
  date.setFullYear(date.getFullYear() - 17);

  $('#birthdate').datepicker('option', 'maxDate', date);
  $('#birthdate').datepicker("setDate", date);
  $("#last_education").select2();
  $("#last_position").select2();
  $("#applied_positions").select2();
  $("#skill").select2();
});
</script>
