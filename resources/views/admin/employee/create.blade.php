@extends("layout.template")

@push('custom-css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<style>
    .swal-modal{
        width: 375px !important;
    }
    .swal-footer{
        text-align: center !important;
    }
    .img-preview{
      width: 5rem;
    }
    .address {
      resize: none;
    }
</style>
@endpush

@section('content')

@if (old())
  {{-- expr --}}
  {{-- @dd(old()) --}}
@endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                <h1>{{ (@$data) ? 'Ubah' : 'Buat' }} Karyawan</h1>
                </div>
            </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                          <form action="{{ (@$data) ? route('employee.update', encrypt(@$data->id)) : route('employee.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                              <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nama <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control" id="name" value="{{ (@$data->name) ?? old('name') }}" placeholder="Nama" required>
                                  {!! @$errors ? $errors->first('name', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-name"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="role_id" class="col-sm-2 col-form-label">Posisi <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="role_id" class="form-control" id="role_id" required>
                                    <option value="" disabled {{ (@$data->gender) ?? 'selected' }}>Pilih Posisi</option>
                                      @foreach (@$roles as $key => $role)
                                          <option value="{{ $role->id }}" {{ ((old('role_id') == $role->id) || ((@$data->user->role_id) == $role->id)) ? 'selected' : '' }} >{{ $role->name }}</option>
                                      @endforeach
                                  </select>
                                  {!! @$errors ? $errors->first('role_id', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-role_id"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Alamat Email</label>
                                <div class="col-sm-10">
                                  <input type="email" name="email" class="form-control" value="{{ (@$data->user->email) ?? old('email') }}" id="email" placeholder="Alamat Email">
                                  {!! @$errors ? $errors->first('email', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-email"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="address" class="col-sm-2 col-form-label">Alamat <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <textarea name="address" class="form-control address" id="address" placeholder="Alamat" required cols="3" rows="3">{{ (@$data->address) ?? old('address') }}</textarea>
                                  {!! @$errors ? $errors->first('address', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-address"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="phone_number" class="col-sm-2 col-form-label">No Telepon <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <input type="tel" name="phone_number" class="form-control" id="phone_number"  maxlength="13" minlength="10" value="{{ (@$data->phone_number) ?? old('phone_number') }}" placeholder="No Telepon" required>
                                  {!! @$errors ? $errors->first('phone_number', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-phone_number"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="gender" class="col-sm-2 col-form-label">Jenis Kelamin <span class="required-field-sign"></span></label>
                                <div class="col-sm-10">
                                  <select name="gender" class="form-control" id="gender" required>
                                    <option value="" disabled {{ (@$data->gender) ?? 'selected' }}>Jenis Kelamin</option>
                                    <option value="male" {{ ((old('gender') == "male") || ((@$data->gender) == "male")) ? 'selected' : '' }}>Pria</option>
                                    <option value="female" {{ ((old('gender') == "female") || ((@$data->gender) == "female")) ? 'selected' : '' }}>Wanita</option>
                                  </select>
                                  {!! @$errors ? $errors->first('gender', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-gender"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password {!! (@$data) ? '' : '<span class="required-field-sign"></span>' !!}</label>
                                <div class="col-sm-10">
                                      <div class="input-group">
                                          <input type="password" name="password" class="form-control border-right-0" id="password" placeholder="Password" {{ @$data ? '' : 'required' }}>
                                          <div class="input-group-append bg-white">
                                              <div class="btn border border-left-0" id="show">
                                                <span class="fas fa-eye"></span>
                                              </div>
                                              <div class="btn border border-left-0" id="hide" hidden>
                                                <span class="fas fa-eye-slash"></span>
                                              </div>
                                          </div>
                                      </div>
                                  {!! @$errors ? $errors->first('password', '<code><small>:message</small></code>') : '' !!}
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="error-password"></strong>
                                    </span>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-2 col-form-label">Konfirmasi Password {!! (@$data) ? '' : '<span class="required-field-sign"></span>' !!}</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="password" name="password_confirmation" class="form-control border-right-0" id="password_confirmation" placeholder="Konfirm Password"  {{ @$data ? '' : 'required' }}>
                                        <div class="input-group-append bg-white">
                                            <div class="btn border border-left-0" id="show_form">
                                              <span class="fas fa-eye"></span>
                                            </div>
                                            <div class="btn border border-left-0" id="hide_form" hidden>
                                              <span class="fas fa-eye-slash"></span>
                                            </div>
                                        </div>
                                    </div>
                                  {!! @$errors ? $errors->first('password_confirmation', '<code><small>:message</small></code>') : '' !!}
                                  <div style="margin-top: 7px;" id="CheckPasswordMatch"></div>
                                </div>
                              </div>
                            </div>

                            <div class="card-footer">
                              <a href="{{ route('employee.index') }}" id="batal" class="btn btn-default">Batal</a>
                              <button type="submit" class="btn btn-primary">{{ (@$data) ? 'Ubah' : 'Simpan' }}</button>
                            </div>
                            <!-- /.card-body -->
                          </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            </section>


    </div>

@endsection

@push('custom-scripts')

<script>
  $(document).ready(function(){
    $("#phone_number").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if (event.which > 31 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#show').on('click', function(){
        $('#show').attr('hidden','true');
        $('#hide').removeAttr('hidden');
        $('#password').attr('type','text');
    })
    $('#hide').on('click', function(){
        $('#show').removeAttr('hidden');
        $('#hide').attr('hidden','true');
        $('#password').attr('type','password');
    })
    $('#show_form').on('click', function(){
        $('#show_form').attr('hidden','true');
        $('#hide_form').removeAttr('hidden');
        $('#password_confirmation').attr('type','text');
    })
    $('#hide_form').on('click', function(){
        $('#show_form').removeAttr('hidden');
        $('#hide_form').attr('hidden','true');
        $('#password_confirmation').attr('type','password');
    })
      $("#password_confirmation").on('keyup', function() {
        var password = $("#password").val();
        var confirmPassword = $("#password_confirmation").val();
        if (password != confirmPassword)
          $("#CheckPasswordMatch").html("Password does not match !").css("color", "red");
        else
          $("#CheckPasswordMatch").html("Password match !").css("color", "green");
      });
  });

</script>

@include("admin.employee.script")
@endpush
