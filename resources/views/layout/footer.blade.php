    @if (!request()->is('login'))
    <footer class="main-footer">
        <strong>Copyright &copy; 2022 <a href="/">{{ env('APP_NAME') }}</a>.</strong> All rights reserved.
    </footer>
    @endif

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('template/') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('template/') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('template/') }}/dist/js/adminlte.min.js"></script>
<script src="{{ asset('template/') }}/plugins/select2/js/select2.min.js"></script>
{{-- <script src=" {{ asset('template/') }}/plugins/sweetalert2/sweetalert2.min.js"></script> --}}

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/') }}/lodash.min.js"></script>
@toastr_js
@toastr_render

<!-- AdminLTE -->
<script>
jQuery('a[data-widget=pushmenu]').click(function(){
    jQuery('.sidebar-mini').toggleClass('sidebar-collapse');
});
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function loading(message) {
        var options = {
            theme:"sk-circle",
            message: message ?? '<br>Proccessing your data, please wait a moment',
        };

        HoldOn.open(options);
    }
    jQuery('a[data-widget=pushmenu]').click(function(){
        jQuery('.sidebar-mini').toggleClass('sidebar-collapse');
    });

    $('#logout').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Logout',
        text: 'Apakah anda yakin akan logout?',
        customClass: 'swal-width',
        icon: 'warning',
        buttons: ["Tidak", "Iya"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script>

@stack('custom-scripts')

</body>
</html>
