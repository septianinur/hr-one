@include('layout.header')

    @if (!request()->is('login'))
        @include('layout.navbar')
    @endif
  
    
    @yield('content')

@include('layout.footer')