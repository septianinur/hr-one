<nav class="main-header navbar navbar-expand navbar-dark border-bottom-0">
  <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    <li class="nav-item">
      <a class="nav-link" href="javascript:void(0)">
        {{ user()->employee->name }}
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i>
      </a>
    </li>
  </ul>
</nav>
<!-- /.navbar -->

<!-- Navbar -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-light elevation-4">
  <!-- Brand Logo -->
  <div class="brand-link d-flex justify-content-between align-items-center">
    <a href="/" class="brand-link">
      <img src="{{ asset('template/') }}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
    </a>
  </div>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">Menu</li>

          @can('home.index')
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link {{ request()->is('*home*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Beranda
              </p>
            </a>
          </li>
          @endcan
          @can('role.index')
          <li class="nav-item">
            <a href="{{ route('role.index') }}" class="nav-link {{ request()->is('*role*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Role
              </p>
            </a>
          </li>
          @endcan
          @can('employee.index')
          <li class="nav-item">
            <a href="{{ route('employee.index') }}" class="nav-link {{ request()->is('*employee*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Karyawan
              </p>
            </a>
          </li>
          @endcan
          @can('candidate.index')
          <li class="nav-item">
            <a href="{{ route('candidate.index') }}" class="nav-link {{ request()->is('*candidate*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Kandidat
              </p>
            </a>
          </li>
          @endcan
          
          <li class="nav-item">
            <a href="{{ url()->route('logout') }}" class="nav-link" id="logout">
              <i class="fas fa-sign-out-alt"></i>
              <p>
                Keluar
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
