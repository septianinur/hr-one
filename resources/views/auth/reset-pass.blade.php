@extends("layout.template")

@section('content')
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="/" class="h1"><b>{{ env('APP_NAME') }}</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Silakan reset password Anda</p>

      <form method="POST" action="{{ route('users.reset-password.store', @$data)}}">
        @csrf
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" name="old_password" placeholder="Old Password" id="old_password" class="form-control border-right-0 @error('old_password') is-invalid @enderror" required autocomplete="off">
          <div class="input-group-append bg-white">
            <div class="btn border border-left-0" onclick="showold()" id="showold">
              <span class="fas fa-eye"></span>
            </div>
            <div class="btn border border-left-0" onclick="hideold()" id="hideold" hidden>
              <span class="fas fa-eye-slash"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-1">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input onkeyup="trigger()" type="password" name="password" placeholder="Password" id="password" class="form-control border-right-0 @error('password') is-invalid @enderror" required autocomplete="off">
          <div class="input-group-append bg-white">
            <div class="btn border border-left-0" onclick="show()" id="show">
              <span class="fas fa-eye"></span>
            </div>
            <div class="btn border border-left-0" onclick="hide()" id="hide" hidden>
              <span class="fas fa-eye-slash"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-4">
          {!! @$errors ? $errors->first('password', '<code><small>:message</small></code>') : '' !!}
          <div class="indicator">
            <span class="weak"></span>
            <span class="medium"></span>
            <span class="strong"></span>
          </div>
          <div class="text"></div>
        </div>

        <div class="input-group mb-1">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input onkeyup="pass_match()" type="password" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation" class="form-control border-right-0 @error('password') is-invalid @enderror" required autocomplete="off">
          <div class="input-group-append bg-white">
            <div class="btn border border-left-0" onclick="show_firm()" id="show_firm">
              <span class="fas fa-eye"></span>
            </div>
            <div class="btn border border-left-0" onclick="hide_firm()" id="hide_firm" hidden>
              <span class="fas fa-eye-slash"></span>
            </div>
          </div>
          {!! @$errors ? $errors->first('password_confirmation', '<code><small>:message</small></code>') : '' !!}
        </div>
        <div class="input-group mb-3">
          <div class="text" id="password_confirmation_msg"></div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-8"></div>
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
@endsection

@push('custom-scripts')
    
    <script>
        function showold() {
            $('#showold').attr('hidden','true');
            $('#hideold').removeAttr('hidden');
            $('#old_password').attr('type','text');
        }
        function hideold(params) {
            $('#showold').removeAttr('hidden');
            $('#hideold').attr('hidden','true');
            $('#old_password').attr('type','password');
        }
        function show() {
            $('#show').attr('hidden','true');
            $('#hide').removeAttr('hidden');
            $('#password').attr('type','text');
        }
        function hide(params) {
            $('#show').removeAttr('hidden');
            $('#hide').attr('hidden','true');
            $('#password').attr('type','password');
        }
        function show_firm() {
            $('#show_firm').attr('hidden','true');
            $('#hide_firm').removeAttr('hidden');
            $('#password_confirmation').attr('type','text');
        }
        function hide_firm(params) {
            $('#show_firm').removeAttr('hidden');
            $('#hide_firm').attr('hidden','true');
            $('#password_confirmation').attr('type','password');
        }

        function pass_match() {
          var password = $("#password").val();
          var confirmPassword = $("#password_confirmation").val();
          if (password != confirmPassword){
            $("#password_confirmation_msg").text("Password tidak sama !");
            $("#password_confirmation_msg").attr('style','color:#ff4757; font-size: 15px; display:block');
          }else{
            $("#password_confirmation_msg").text("Password sama !");
            $("#password_confirmation_msg").attr('style','color:#23ad5c; font-size: 15px; display:block');
          }
          if(confirmPassword.length == 0){
            console.log(confirmPassword.length);
            $("#password_confirmation_msg").attr('style','display:none');
          }
        };

        const indicator = document.querySelector(".indicator");
        const weak = document.querySelector(".weak");
        const medium = document.querySelector(".medium");
        const strong = document.querySelector(".strong");
        const text = document.querySelector(".text");

        let regNumber = /([0-9])/;
        let regLower = /([a-z])/;
        let regUpper = /([A-Z])/;
        let regSpecialChar = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
        function trigger(){
          if($('#password').val() != ""){
            indicator.style.display = "block";
            indicator.style.display = "flex";
            indicator.style.width = "100%";

            if($('#password').val().length <= 12 && (($('#password').val().match(regNumber) || $('#password').val().match(regLower) || $('#password').val().match(regSpecialChar) || $('#password').val().match(regLower) && $('#password').val().match(regSpecialChar))))no=1;
            if($('#password').val().length >= 12 && (($('#password').val().match(regNumber) && $('#password').val().match(regLower)) || ($('#password').val().match(regLower) && $('#password').val().match(regSpecialChar)) || ($('#password').val().match(regNumber) && $('#password').val().match(regSpecialChar)) || ($('#password').val().match(regNumber) && $('#password').val().match(regSpecialChar) && $('#password').val().match(regLower))))no=2;
            if($('#password').val().length >= 12 && ($('#password').val().match(regNumber) && $('#password').val().match(regLower) && $('#password').val().match(regSpecialChar) && $('#password').val().match(regUpper)))no=3;

              if ($('#password').val().length < 12) {
                weak.classList.add("active");
                text.style.display = "block";
                text.textContent = "Lemah (minimal harus 12 karakter.)";
                text.style.fontSize = "15px";
                text.classList.add("weak");
                medium.classList.remove("active");
                text.classList.remove("medium");
                strong.classList.remove("active");
                text.classList.remove("strong");
              }else{
                if(no==1){
                  weak.classList.add("active");
                  text.style.display = "block";
                  text.textContent = "Lemah";
                  text.classList.add("weak");
                }else{
                  medium.classList.remove("active");
                  text.classList.remove("medium");
                }
                if(no==2){
                  medium.classList.add("active");
                  text.textContent = "Sedang (harus menyertakan huruf kapital, angka, dan karakter khusus atau beberapa kombinasi.";
                  text.style.fontSize = "15px";
                  text.classList.add("medium");
                }else{
                  medium.classList.remove("active");
                  text.classList.remove("medium");
                }
                if(no==3){
                  weak.classList.add("active");
                  medium.classList.add("active");
                  strong.classList.add("active");
                  text.textContent = "Kuat";
                  text.style.fontSize = "15px";
                  text.classList.add("strong");
                }else{
                  strong.classList.remove("active");
                  text.classList.remove("strong");
                }
              }
          }else{
            indicator.style.display = "none";
            text.style.display = "none";
          }
        }
    </script>
@endpush

