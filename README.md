<p  align="center"><a  href="https://laravel.com"  target="_blank"><img  src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg"  width="400"></a></p>

  
<p  align="center">

<a  href="https://packagist.org/packages/laravel/framework"><img  src="https://img.shields.io/packagist/v/laravel/framework"  alt="Latest Stable Version"></a>

<a  href="https://packagist.org/packages/laravel/framework"><img  src="https://img.shields.io/packagist/l/laravel/framework"  alt="License"></a>

</p>

  

## HR-ONE

  

HR-ONE is a web to manage candidate list by HR team
 

## Requirements  
-  **[PHP > 7.4](https://64robots.com)**

-  **[MySQL](https://www.mysql.com/)**
  
-  **[Composer](https://getcomposer.org/)**

## Installation

  Clone the repository

    git clone [our_git_url]

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Generate laravel-passport key

    php artisan passport:keys
    php artisan passport:install

Run the database migration and the seeder

    php artisan migrate:fresh --seed

Run Application

    php artisan serve 
    
    Or run using Virtualhost 

    PATH_PROJECT/public
